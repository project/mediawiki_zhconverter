/**
 *  Mediawiki ZH Converter
 *  Update: 2012/02/10
 */


Summary
-------
Base on Mediawiki(http://www.mediawiki.org/wiki/MediaWiki)
and mediawiki-zhconverter which is from google project (http://code.google.com/p/mediawiki-zhconverter/)
This module can provide other modules to use. You can translated some content into other languages.

Example: content "資料庫" could be translated to "数据库".
Demo: http://labs.xddnet.com/mediawiki-zhconverter/example/example.html



Related Modules
---------------

 * Cross Chinese search --- http://drupal.org/project/ccsearch


Installation and Settings
-------------------------
TO successful completion of this installation, according to the following steps:

1) Go to mediawiki-zhconverter website (http://code.google.com/p/mediawiki-zhconverter/)
   and download this project.
   [!Notice]: Pay attention mediawiki-zhconverter website Supported versions for MediaWiki.
   
2) Go to Mediawiki website (http://www.mediawiki.org/wiki/MediaWiki)
   and download the project which is support for mediawiki-zhconverter

3) Unpackage above two files and rename the folder.

   a) Download from Mediawiki site will be "mediawiki".
      ex. "mediawiki-1.15.4" to "mediawiki"
   
   b) Download from mediawiki-zhconverter site will be "zhconverter".
      ex. "tszming-mediawiki-zhconverter-98f88f1" to "zhconverter"

4) Check above two folder's name is correct and copy to this module root 
   (ex. /sites/all/modules/mediawiki_zhconverter).

5) Enable this module.
   
   
Author
------
Jimmy Huang (jimmy@jimmyhub.net, http://drupaltaiwan.org, user name jimmy)
If you use this module, find it useful, and want to send the author a thank 
or you note, feel free to contact me.

The author can also be contacted for paid customizations of this and other modules.